# 3D Animation JS 🧊
---
I created this 3D animation for practice and fun

### What does the project look like?
![index](https://bytebucket.org/nicobxez/3d-animation-js/raw/dae046ecc55f504cdb54276150bde76bde30abd4/3d-animation-js.gif)

---
⌨️ with ❤️ by [nicobxez](https://bitbucket.org/nicobxez/) 😊