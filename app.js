//Movement Animation to happen
const container = document.querySelector('.container');
const card = document.querySelector('.card');
//Items
const title = document.querySelector('.title');
const img = document.getElementById('img');
const purchase = document.querySelector('.purchase');
const description = document.querySelector('.info h3');
const colors = document.querySelector('.colors');
const goldButton = document.getElementById('goldButton');
const silverButton = document.getElementById('silverButton');
const blackButton = document.getElementById('blackButton');
const pinkButton = document.getElementById('pinkButton');
const mark = document.querySelector('.mark');

// console.log(mark[0]);

//Moving Animation Event
container.addEventListener('mousemove', (e) => {
    let xAxis = (window.innerWidth / 2 - e.pageX) / 25;
    let yAxis = (window.innerHeight / 2 - e.pageY) / 25;
    card.style.transform = `rotateY(${xAxis}deg) rotateX(${yAxis}deg)`;
});

//Animate In
container.addEventListener('mouseenter', () => {
    card.style.transition = 'none';
    //PopOut
    title.style.transform = 'translateZ(150px)';
    img.style.transform = 'translateZ(200px) rotateZ(24deg)';
    description.style.transform = 'translateZ(125px)';
    colors.style.transform = 'translateZ(100px)';
    purchase.style.transform = 'translateZ(75px)';
});

//Animate Out
container.addEventListener('mouseleave', () => {
    card.style.transition = 'all 0.5s ease';
    card.style.transform = `rotateY(0deg) rotateX(0deg)`;
    //PopBack
    title.style.transform = 'translateZ(0px)';
    img.style.transform = 'translateZ(0px) rotateZ(0deg)';
    description.style.transform = 'translateZ(0px)';
    colors.style.transform = 'translateZ(0px)';
    purchase.style.transform = 'translateZ(0px)';
});

// Color pickers
goldButton.addEventListener('click', () => {
    for (purchaseActive of purchase.classList) { purchaseActive.includes('Active') ? purchase.classList.remove(purchaseActive) : null; };
    for (index in mark.childNodes) { index % 2 == 0 ? mark.childNodes[index].style.color = 'khaki' : null; };
    blackButton.classList.remove('blackActive');
    pinkButton.classList.remove('pinkActive');
    silverButton.classList.remove('silverActive');
    goldButton.classList.add('goldActive');
    purchase.classList.add('goldActive');
    img.src = 'img/gold_watch.png';
});

silverButton.addEventListener('click', () => {
    for (purchaseActive of purchase.classList) { purchaseActive.includes('Active') ? purchase.classList.remove(purchaseActive) : null; };
    for (index in mark.childNodes) { index % 2 == 0 ? mark.childNodes[index].style.color = 'rgb(165, 165, 165)' : null; };
    goldButton.classList.remove('goldActive');
    blackButton.classList.remove('blackActive');
    pinkButton.classList.remove('pinkActive');
    silverButton.classList.add('silverActive');
    purchase.classList.add('silverActive');
    img.src = 'img/silver_watch.png';
});

blackButton.addEventListener('click', () => {
    for (purchaseActive of purchase.classList) { purchaseActive.includes('Active') ? purchase.classList.remove(purchaseActive) : null; };
    for (index in mark.childNodes) { index % 2 == 0 ? mark.childNodes[index].style.color = 'black' : null; };
    goldButton.classList.remove('goldActive');
    pinkButton.classList.remove('pinkActive');
    silverButton.classList.remove('silverActive');
    blackButton.classList.add('blackActive');
    purchase.classList.add('blackActive');
    img.src = 'img/black_watch.png';
});

pinkButton.addEventListener('click', () => {
    for (purchaseActive of purchase.classList) { purchaseActive.includes('Active') ? purchase.classList.remove(purchaseActive) : null; };
    for (index in mark.childNodes) { index % 2 == 0 ? mark.childNodes[index].style.color = ' rgb(253, 205, 213)' : null; };
    goldButton.classList.remove('goldActive');
    silverButton.classList.remove('silverActive');
    blackButton.classList.remove('blackActive');
    pinkButton.classList.add('pinkActive');
    purchase.classList.add('pinkActive');
    img.src = 'img/pink_watch.png';
});